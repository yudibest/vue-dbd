import checkingData from "../api/questions.json";

const checkingDb = {
  methods: {
    checkingUserAction(id, answerType) {
      localStorage.setItem("historyAnswerId", id);
      setTimeout(() => {
        if (id == 0 && answerType == "Tidak") {
          this.pushThanks();
        } else if (id === 0 && answerType === "Ya") {
          this.pushQuestions(id, "questions");
        }

        // chek panas naik turun
        if (id === 1 && answerType === "Tidak") {
          this.pushQuestions(0, "action");
        } else if (id === 1 && answerType === "Ya") {
          this.pushQuestions(id, "questions");
        }

        // is user muntah muntah
        if (id === 2 && answerType === "Tidak") {
          this.pushQuestions(id, "questions");
        } else if (id === 2 && answerType === "Ya") {
          this.pushQuestions(3, "questions");
          this.isNotLowEnergi = true;
        }

        //lemas
        if (id === 3 && answerType === "Tidak") {
          if (this.isNotLowEnergi) {
            this.pushQuestions(4, "questions");
          } else {
            this.pushQuestions(2, "action");
            this.isReadyForAnswer = false;
          }
        } else if (id === 3 && answerType === "Ya") {
          this.pushQuestions(3, "positif");
          this.isReadyForAnswer = false;
          this.clearHistory();
        }

        //nyeri
        if (id === 4 && answerType === "Tidak") {
          this.pushQuestions(5, "questions");
        } else if (id === 4 && answerType === "Ya") {
          this.pushQuestions(4, "positif");
          this.isReadyForAnswer = false;
          this.clearHistory();
        }

        //makan
        if (id === 5 && answerType === "Tidak") {
          this.pushQuestions(6, "questions");
        } else if (id === 5 && answerType === "Ya") {
          this.pushQuestions(5, "positif");
          this.isReadyForAnswer = false;
          this.clearHistory();
        }

        //bintik merah
        if (id === 6 && answerType === "Tidak") {
          this.pushQuestions(6, "positif");
          this.isReadyForAnswer = false;
          this.clearHistory();
        } else if (id === 6 && answerType === "Ya") {
          this.pushQuestions(7, "questions");
        }

        //pusing
        if (id === 7 && answerType === "Tidak") {
          this.pushQuestions(7, "action");
          this.pushQuestions(7, "action2");
          this.isReadyForAnswer = false;
          this.clearHistory();
        } else if (id === 7 && answerType === "Ya") {
          this.pushQuestions(7, "positif");
          this.isReadyForAnswer = false;
          this.clearHistory();
        }
      }, 1000);
    },

    pushQuestions(id, answer) {
      console.log(id, answer);
      let questions;
      if (answer === "action") {
        questions = {
          type: "questions",
          content: checkingData.data[id].action,
        };
        this.isReadyForAnswer = false;
      } else if (answer === "questions") {
        questions = {
          type: "questions",
          content: checkingData.data[id].questions,
        };
      } else {
        questions = {
          type: "questions",
          content: checkingData.data[id].positif,
        };
      }

      this.chatConversation.push(questions);
      this.scrollPage();
    },

    pushThanks() {
      const questions = {
        type: "questions",
        content:
          "Sampai jumpa di lain waktu , kami akan selalu siap membantu anada untuk mengetahui gejala DBD Terimakasih !",
      };

      this.chatConversation.push(questions);
      this.scrollPage();
      this.isReadyForAnswer = false;
    },

    clearHistory() {
      localStorage.removeItem("backupChat");
    },
  },
};

export default checkingDb;
